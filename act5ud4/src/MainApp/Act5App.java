/**
 * 
 */
package MainApp;

/**
 * @author Elisabet Sabat�
 *
 */
public class Act5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//Les variables a2 i b2 s�n variables temporals per tal de conservar el valor de a per a poder atribuir-lo a altres variables. 
		int A = 1, A2 = A, B = 2, B2 = B, C = 3, D = 4;
		
		System.out.println("Valors inicials: A = " + A + ", B = " + B + ", C = " + C + ", D = " + D);
		
		B = C; 
		C = A2;
		A = D;
		D = B2;
		
		System.out.println("Valors finals: A = " + A + ", B = " + B + ", C = " + C + ", D = " + D);
		
		
	}

}
